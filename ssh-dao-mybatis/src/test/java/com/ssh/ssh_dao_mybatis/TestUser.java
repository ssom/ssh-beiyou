package com.ssh.ssh_dao_mybatis;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ssh.dao.UserMapper;
import com.ssh.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/spring-persistence.xml")
public class TestUser {
	
	@Autowired
	private UserMapper userMapper;
	
	@Test
	public void testAdd() {
		User user = new User();
		user.setId(new Date().getTime());
		user.setUsername("aaaa");
		user.setPassword("bbbb");
		user.setName("adfaf");
		userMapper.insert(user);
	}
	
}
