package com.ssh.dao;

import com.ssh.model.RolePermission;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface RolePermissionMapper {
    @Delete({
        "delete from t_role_permission",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);
    
    @Delete({
    	"delete from t_role_permission",
    	"where roleId=#{roleId} and permissionId=#{permissionId}"
    })
    public int deleteByRoleIdAndPermissionId(Long roleId,Long permissionId);

    @Insert({
        "insert into t_role_permission (id, permissionId, ",
        "roleId)",
        "values (#{id,jdbcType=BIGINT}, #{permissionid,jdbcType=BIGINT}, ",
        "#{roleid,jdbcType=BIGINT})"
    })
    int insert(RolePermission record);

    int insertSelective(RolePermission record);

    @Select({
        "select",
        "id, permissionId, roleId",
        "from t_role_permission",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.RolePermissionMapper.BaseResultMap")
    RolePermission selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RolePermission record);

    @Update({
        "update t_role_permission",
        "set permissionId = #{permissionid,jdbcType=BIGINT},",
          "roleId = #{roleid,jdbcType=BIGINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(RolePermission record);
    
    public List<Map<String, Object>> selectRolePermissions(Long roleId);
}