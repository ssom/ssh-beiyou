package com.ssh.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ssh.model.Permission;

public interface PermissionMapper {
    @Delete({
        "delete from t_permission",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into t_permission (id, name, ",
        "description)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{description,jdbcType=VARCHAR})"
    })
    int insert(Permission record);

    int insertSelective(Permission record);

    @Select({
        "select",
        "id, name, description",
        "from t_permission",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.PermissionMapper.BaseResultMap")
    Permission selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Permission record);

    @Update({
        "update t_permission",
        "set name = #{name,jdbcType=VARCHAR},",
        "description = #{description,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Permission record);
    
    @Select({
    	"select * from t_permission"
    })
    @ResultMap("com.ssh.dao.PermissionMapper.BaseResultMap")
    public List<Permission> findAll();
    
    public List<String> selectUserPermissions(String username);
}