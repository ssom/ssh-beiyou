package com.ssh.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ssh.model.Order;

public interface OrderMapper {
    @Delete({
        "delete from t_order",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into t_order (id, orderPrice, ",
        "paymentId, status, ",
        "userId)",
        "values (#{id,jdbcType=BIGINT}, #{orderprice,jdbcType=INTEGER}, ",
        "#{paymentid,jdbcType=BIGINT}, #{status,jdbcType=INTEGER}, ",
        "#{userid,jdbcType=BIGINT})"
    })
    int insert(Order record);

    int insertSelective(Order record);

    @Select({
        "select",
        "id, orderPrice, paymentId, status, userId",
        "from t_order",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.OrderMapper.BaseResultMap")
    Order selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Order record);

    @Update({
        "update t_order",
        "set orderPrice = #{orderprice,jdbcType=INTEGER},",
          "paymentId = #{paymentid,jdbcType=BIGINT},",
          "status = #{status,jdbcType=INTEGER},",
          "userId = #{userid,jdbcType=BIGINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Order record);
    
    @Select({
    	"select * from t_order"
    })
    @ResultMap("com.ssh.dao.OrderMapper.BaseResultMap")
    public List<Order> findAll();
    
    @Select({
    	"select * from t_order where userId=#{userId}"
    })
    @ResultMap("com.ssh.dao.OrderMapper.BaseResultMap")
    public List<Order> findByUserid(Long userId);
}