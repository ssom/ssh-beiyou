package com.ssh.dao;

import com.ssh.model.PermissionResource;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface PermissionResourceMapper {
    @Delete({
        "delete from t_permission_resource",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);
    
    @Delete({
    	"delete from t_permission_resource",
    	"where permissionId=#{pid} and resourceId=#{rid}"
    })
    public int deleteByPidAndRid(Long pid,Long rid);

    @Insert({
        "insert into t_permission_resource (id, permissionId, ",
        "resourceId)",
        "values (#{id,jdbcType=BIGINT}, #{permissionid,jdbcType=BIGINT}, ",
        "#{resourceid,jdbcType=BIGINT})"
    })
    int insert(PermissionResource record);

    int insertSelective(PermissionResource record);

    @Select({
        "select",
        "id, permissionId, resourceId",
        "from t_permission_resource",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.PermissionResourceMapper.BaseResultMap")
    PermissionResource selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PermissionResource record);

    @Update({
        "update t_permission_resource",
        "set permissionId = #{permissionid,jdbcType=BIGINT},",
          "resourceId = #{resourceid,jdbcType=BIGINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(PermissionResource record);
    
    public List<Map<String, Object>> selectPermissionResources(@Param("permissionId") Long permissionId);
}