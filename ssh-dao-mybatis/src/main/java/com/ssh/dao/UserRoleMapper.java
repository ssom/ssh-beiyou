package com.ssh.dao;

import com.ssh.model.UserRole;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface UserRoleMapper {
    @Delete({
        "delete from t_user_role",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);
    
    /**
     * 根据userId和roleId删除一条记录
     * @param userId
     * @param roleId
     * @return
     */
    @Delete({
    	"delete from t_user_role where userId=#{userId} and roleId=#{roleId}"
    })
    public int deleteByUserIdAndRoleId(@Param("userId") Long userId,@Param("roleId") Long roleId);

    @Insert({
        "insert into t_user_role (id, roleId, ",
        "userId)",
        "values (#{id,jdbcType=BIGINT}, #{roleid,jdbcType=BIGINT}, ",
        "#{userid,jdbcType=BIGINT})"
    })
    int insert(UserRole record);

    int insertSelective(UserRole record);

    @Select({
        "select",
        "id, roleId, userId",
        "from t_user_role",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.UserRoleMapper.BaseResultMap")
    UserRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserRole record);

    @Update({
        "update t_user_role",
        "set roleId = #{roleid,jdbcType=BIGINT},",
          "userId = #{userid,jdbcType=BIGINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(UserRole record);
    /**
     * 根据用户id查询用户所有的角色
     * @param userId
     * @return
     */
    public List<Map<String, Object>> selectUserRoles(@Param("userId") Long userId);
    
}