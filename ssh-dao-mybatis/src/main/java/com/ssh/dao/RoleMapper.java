package com.ssh.dao;

import com.ssh.model.Role;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface RoleMapper {
    @Delete({
        "delete from t_role",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into t_role (id, name, ",
        "description)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{description,jdbcType=VARCHAR})"
    })
    int insert(Role record);

    int insertSelective(Role record);

    @Select({
        "select",
        "id, name, description",
        "from t_role",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.RoleMapper.BaseResultMap")
    Role selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Role record);

    @Update({
        "update t_role",
        "set name = #{name,jdbcType=VARCHAR},",
          "description = #{description,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Role record);
    
    @Select({
    	"select * from t_role"
    })
    @ResultMap("com.ssh.dao.RoleMapper.BaseResultMap")
    public List<Role> selectByAll();
    
    @Select({
    	"SELECT r.name FROM t_user_role ur "+
    	"LEFT JOIN t_user u ON(ur.userId=u.id) "+
    	"LEFT JOIN t_role r ON(ur.roleId=r.id) "+
    	"WHERE u.username=#{username}"
    })
    public List<String> selectByUsername(String username);
}