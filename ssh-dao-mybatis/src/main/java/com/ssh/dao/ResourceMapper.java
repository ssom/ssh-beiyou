package com.ssh.dao;

import com.ssh.model.Resource;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface ResourceMapper {
    @Delete({
        "delete from t_resource",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into t_resource (id, name, ",
        "url, description)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{url,jdbcType=VARCHAR}, #{description,jdbcType=VARCHAR})"
    })
    int insert(Resource record);

    int insertSelective(Resource record);

    @Select({
        "select",
        "id, name, url, description",
        "from t_resource",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.ResourceMapper.BaseResultMap")
    Resource selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Resource record);

    @Update({
        "update t_resource",
        "set name = #{name,jdbcType=VARCHAR},",
          "url = #{url,jdbcType=VARCHAR},",
          "description = #{description,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Resource record);
    
    @Select({
    	"select * from t_resource"
    })
    @ResultMap("com.ssh.dao.ResourceMapper.BaseResultMap")
    public List<Resource> findAll();
}