package com.ssh.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ssh.model.User;

public interface UserMapper {
    @Delete({
        "delete from t_user",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into t_user (id, name, ",
        "password, username)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{password,jdbcType=VARCHAR}, #{username,jdbcType=VARCHAR})"
    })
    int insert(User record);

    int insertSelective(User record);

    @Select({
        "select",
        "id, name, password, username",
        "from t_user",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.UserMapper.BaseResultMap")
    User selectByPrimaryKey(Long id);
    
    @Select({
    	"select * from t_user where username=#{username}"
    })
    @ResultMap("com.ssh.dao.UserMapper.BaseResultMap")
    public User selectByUsername(String username);
    
    @Select({
    	"select * from t_user"
    })
    @ResultMap("com.ssh.dao.UserMapper.BaseResultMap")
    public List<User> selectAll();

    int updateByPrimaryKeySelective(User record);

    @Update({
        "update t_user",
        "set name = #{name,jdbcType=VARCHAR},",
          "password = #{password,jdbcType=VARCHAR},",
          "username = #{username,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(User record);
}