package com.ssh.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ssh.model.Commodity;

public interface CommodityMapper {
    @Delete({
        "delete from t_commodity",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into t_commodity (id, name, ",
        "number, origin, ",
        "price)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{number,jdbcType=INTEGER}, #{origin,jdbcType=VARCHAR}, ",
        "#{price,jdbcType=INTEGER})"
    })
    int insert(Commodity record);

    int insertSelective(Commodity record);

    @Select({
        "select",
        "id, name, number, origin, price",
        "from t_commodity",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("com.ssh.dao.CommodityMapper.BaseResultMap")
    Commodity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Commodity record);

    @Update({
        "update t_commodity",
        "set name = #{name,jdbcType=VARCHAR},",
          "number = #{number,jdbcType=INTEGER},",
          "origin = #{origin,jdbcType=VARCHAR},",
          "price = #{price,jdbcType=INTEGER}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Commodity record);
    
    @Select({
    	"select * from t_commodity"
    })
    @ResultMap("com.ssh.dao.CommodityMapper.BaseResultMap")
    public List<Commodity> findAll();
}