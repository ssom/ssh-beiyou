package com.ssh.jpa.dao.user;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.user.IUserRoleRepository;

public interface IUserRoleDao extends IBaseDao<IUserRoleRepository> {
	
}
