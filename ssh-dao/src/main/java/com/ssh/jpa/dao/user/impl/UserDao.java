package com.ssh.jpa.dao.user.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.user.IUserDao;
import com.ssh.jpa.repository.user.IUserRepository;

@Repository
public class UserDao extends RepositoryFactoryBean<IUserRepository> implements IUserDao {
	
	@Override
	public List<Map<String, Object>> selectUserRoles(Long userId) {
		String sqlString="SELECT role.id as roleId,role.name,ur.userId, " +
					"CASE " +
						"WHEN ur.id IS NULL THEN 0 " +
						"WHEN ur.id IS NOT NULL then 1 " +
					"END as isCheck " +
					"FROM t_role role LEFT JOIN (SELECT * FROM t_user_role WHERE userId=:userId) ur ON(role.id=ur.roleId)";
		Query query = this.entityManager.createNativeQuery(sqlString);
		query.setParameter("userId", userId);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return query.getResultList();
	}

	@Override
	public String add() {
		return null;
	}

}
