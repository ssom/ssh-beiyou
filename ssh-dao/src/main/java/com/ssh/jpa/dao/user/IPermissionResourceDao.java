package com.ssh.jpa.dao.user;

import java.util.List;
import java.util.Map;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.user.IPermissionResourceRepository;

public interface IPermissionResourceDao extends IBaseDao<IPermissionResourceRepository> {

	/**
	 * 查询权限所有的资源
	 * @param permissionId
	 * @return
	 */
	public List<Map<String,Object>> selectPermissionResources(Long permissionId);
	
}
