package com.ssh.jpa.dao.user;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.user.IResourceRepository;

public interface IResourceDao extends IBaseDao<IResourceRepository> {

}
