package com.ssh.jpa.dao.shop.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.shop.ICommodityClassifyDao;
import com.ssh.jpa.repository.shop.ICommodityClassifyRepository;

@Repository
public class CommodityClassifyDao extends RepositoryFactoryBean<ICommodityClassifyRepository> implements ICommodityClassifyDao {

}
