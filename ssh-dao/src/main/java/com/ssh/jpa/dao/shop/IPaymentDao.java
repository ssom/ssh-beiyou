package com.ssh.jpa.dao.shop;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.shop.IPaymentRepository;

public interface IPaymentDao extends IBaseDao<IPaymentRepository> {

}
