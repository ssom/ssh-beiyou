package com.ssh.jpa.dao.user.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.user.IPermissionDao;
import com.ssh.jpa.repository.user.IPermissionRepository;

@Repository
public class PermissionDao extends RepositoryFactoryBean<IPermissionRepository> implements IPermissionDao {

	@Override
	public List<String> selectUserPermissions(String username) {
		String sql = "SELECT p.name\n" +
				"FROM t_role_permission rp\n" +
				"LEFT JOIN t_role r ON(rp.roleId=r.id)\n" +
				"LEFT JOIN t_user_role ur ON(r.id=ur.roleId)\n" +
				"LEFT JOIN t_user u ON(ur.userId=u.id)\n" +
				"LEFT JOIN t_permission p ON(rp.permissionId=p.id)\n" +
				"WHERE u.username=:username";
		Query query = this.entityManager.createNativeQuery(sql);
		query.setParameter("username", username);
		return query.getResultList();
	}

}
