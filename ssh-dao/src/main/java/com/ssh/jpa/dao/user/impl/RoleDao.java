package com.ssh.jpa.dao.user.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.user.IRoleDao;
import com.ssh.jpa.repository.user.IRoleRepository;

@Repository
public class RoleDao extends RepositoryFactoryBean<IRoleRepository> implements IRoleDao {

	@Override
	public List<String> selectUserRoles(String username) {
		String sql = "SELECT r.name FROM t_user_role ur LEFT JOIN t_user u ON(ur.userId=u.id) LEFT JOIN t_role r ON(ur.roleId=r.id) WHERE u.username=:username";
		Query query = this.entityManager.createNativeQuery(sql);
		query.setParameter("username", username);
		return query.getResultList();
	}

}
