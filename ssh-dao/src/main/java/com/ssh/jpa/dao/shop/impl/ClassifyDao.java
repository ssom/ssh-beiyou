package com.ssh.jpa.dao.shop.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.shop.IClassifyDao;
import com.ssh.jpa.repository.shop.IClassifyRepository;

@Repository
public class ClassifyDao extends RepositoryFactoryBean<IClassifyRepository> implements IClassifyDao {


}
