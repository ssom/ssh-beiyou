package com.ssh.jpa.dao.shop;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.shop.ICommodityClassifyRepository;

public interface ICommodityClassifyDao extends IBaseDao<ICommodityClassifyRepository> {

}
