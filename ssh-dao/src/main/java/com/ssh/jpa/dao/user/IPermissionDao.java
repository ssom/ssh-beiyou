package com.ssh.jpa.dao.user;

import java.util.List;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.user.IPermissionRepository;

public interface IPermissionDao extends IBaseDao<IPermissionRepository>{

	public List<String> selectUserPermissions(String username);
	
}
