package com.ssh.jpa.dao.user;

import java.util.List;
import java.util.Map;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.user.IUserRepository;

public interface IUserDao extends IBaseDao<IUserRepository> {
	
	public List<Map<String,Object>> selectUserRoles(Long userId);
	
	public String add();
	
}
