package com.ssh.jpa.dao.shop.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.shop.IPaymentDao;
import com.ssh.jpa.repository.shop.IPaymentRepository;

@Repository
public class PaymentDao extends RepositoryFactoryBean<IPaymentRepository> implements IPaymentDao {

}
