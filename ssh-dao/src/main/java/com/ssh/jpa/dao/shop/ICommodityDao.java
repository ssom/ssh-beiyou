package com.ssh.jpa.dao.shop;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.shop.ICommodityRepository;

public interface ICommodityDao extends IBaseDao<ICommodityRepository> {

}
