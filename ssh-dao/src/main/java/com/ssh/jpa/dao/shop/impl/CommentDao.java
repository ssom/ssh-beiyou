package com.ssh.jpa.dao.shop.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.shop.ICommentDao;
import com.ssh.jpa.repository.shop.ICommentRepository;

@Repository
public class CommentDao extends RepositoryFactoryBean<ICommentRepository> implements ICommentDao {

}
