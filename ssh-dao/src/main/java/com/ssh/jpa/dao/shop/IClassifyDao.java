package com.ssh.jpa.dao.shop;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.shop.IClassifyRepository;

public interface IClassifyDao extends IBaseDao<IClassifyRepository> {

}
