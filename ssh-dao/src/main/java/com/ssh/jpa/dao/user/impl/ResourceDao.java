package com.ssh.jpa.dao.user.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.user.IResourceDao;
import com.ssh.jpa.repository.user.IResourceRepository;

@Repository
public class ResourceDao extends RepositoryFactoryBean<IResourceRepository> implements IResourceDao {

}
