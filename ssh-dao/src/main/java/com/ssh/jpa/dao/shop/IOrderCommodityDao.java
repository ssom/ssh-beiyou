package com.ssh.jpa.dao.shop;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.shop.IOrderCommodityRepository;

public interface IOrderCommodityDao extends IBaseDao<IOrderCommodityRepository> {

}
