package com.ssh.jpa.dao.shop.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.shop.IOrderDao;
import com.ssh.jpa.repository.shop.IOrderRepository;

@Repository
public class OrderDao extends RepositoryFactoryBean<IOrderRepository> implements IOrderDao {

}
