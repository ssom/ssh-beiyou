package com.ssh.jpa.dao.user;

import java.util.List;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.user.IRoleRepository;

public interface IRoleDao extends IBaseDao<IRoleRepository> {
	
	/**
	 * 根据用户id查询一个用户
	 * @param userId
	 * @return
	 */
	public List<String> selectUserRoles(String username);
	
}
