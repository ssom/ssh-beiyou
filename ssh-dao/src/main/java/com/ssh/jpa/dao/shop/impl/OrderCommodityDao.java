package com.ssh.jpa.dao.shop.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.shop.IOrderCommodityDao;
import com.ssh.jpa.repository.shop.IOrderCommodityRepository;

@Repository
public class OrderCommodityDao extends RepositoryFactoryBean<IOrderCommodityRepository> implements IOrderCommodityDao {

}
