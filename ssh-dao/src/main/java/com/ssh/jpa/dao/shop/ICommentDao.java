package com.ssh.jpa.dao.shop;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.shop.ICommentRepository;

public interface ICommentDao extends IBaseDao<ICommentRepository> {

}
