package com.ssh.jpa.dao.shop.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.shop.ICommodityDao;
import com.ssh.jpa.repository.shop.ICommodityRepository;

@Repository
public class CommodityDao extends RepositoryFactoryBean<ICommodityRepository> implements ICommodityDao {

}
