package com.ssh.jpa.dao.user.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.user.IRolePermissionDao;
import com.ssh.jpa.repository.user.IRolePermissionRepository;

@Repository
public class RolePermissionDao extends RepositoryFactoryBean<IRolePermissionRepository> implements IRolePermissionDao {

	@Override
	public List<Map<String, Object>> selectRolePermissions(Long roleId) {
		String sql = "SELECT \n" +
				"	p.id AS permissionId,\n" +
				"	p.name AS name,\n" +
				"	rp.id AS rpId,\n" +
				"	rp.roleId AS roleId,\n" +
				"	CASE\n" +
				"		WHEN rp.id IS NULL THEN 0\n" +
				"		WHEN rp.id IS NOT NULL THEN 1\n" +
				"	END AS isCheck\n" +
				"FROM t_permission p LEFT JOIN (SELECT * FROM t_role_permission WHERE roleId=:roleId) rp ON(p.id=rp.permissionId)";
		Query query = this.entityManager.createNativeQuery(sql);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("roleId", roleId);
		return this.setParamAndMap(query, map).getResultList();
	}
	
}
