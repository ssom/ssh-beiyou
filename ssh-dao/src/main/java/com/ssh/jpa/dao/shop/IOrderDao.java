package com.ssh.jpa.dao.shop;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.shop.IOrderRepository;

public interface IOrderDao extends IBaseDao<IOrderRepository> {

}
