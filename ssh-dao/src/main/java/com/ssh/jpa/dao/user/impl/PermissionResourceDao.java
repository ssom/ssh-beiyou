package com.ssh.jpa.dao.user.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.user.IPermissionResourceDao;
import com.ssh.jpa.repository.user.IPermissionResourceRepository;

@Repository
public class PermissionResourceDao extends RepositoryFactoryBean<IPermissionResourceRepository> implements IPermissionResourceDao {

	@Override
	public List<Map<String, Object>> selectPermissionResources(Long permissionId) {
		String sql = "SELECT\n" +
				"	r.id AS rId,\n" +
				"	r.name AS name,\n" +
				"	r.url AS url,\n" +
				"	pr.id AS prId,\n" +
				"	pr.permissionId AS pId,\n" +
				"	CASE\n" +
				"		WHEN pr.id IS NULL THEN 0\n" +
				"		WHEN pr.id IS NOT NULL THEN 1\n" +
				"	END AS isCheck\n" +
				"FROM t_resource r\n" +
				"LEFT JOIN (\n" +
				"	SELECT * FROM t_permission_resource WHERE permissionId=:permissionId\n" +
				") pr ON (r.id = pr.resourceId)";
		
		Query query = this.entityManager.createNativeQuery(sql);
		Map<String,Object> map = new HashMap<>();
		map.put("permissionId", permissionId);
		return this.setParamAndMap(query, map).getResultList();
	}
	
}
