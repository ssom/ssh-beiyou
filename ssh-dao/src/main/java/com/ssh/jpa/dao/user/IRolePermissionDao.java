package com.ssh.jpa.dao.user;

import java.util.List;
import java.util.Map;

import com.ssh.common.jpa.IBaseDao;
import com.ssh.jpa.repository.user.IRolePermissionRepository;

public interface IRolePermissionDao extends IBaseDao<IRolePermissionRepository> {

	/**
	 * 查询角色的所有权限
	 * @param roleId		角色id
	 * @return
	 */
	public List<Map<String,Object>> selectRolePermissions(Long roleId);
	
}
