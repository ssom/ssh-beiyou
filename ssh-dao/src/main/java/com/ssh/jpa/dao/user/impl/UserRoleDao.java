package com.ssh.jpa.dao.user.impl;

import org.springframework.stereotype.Repository;

import com.ssh.common.jpa.RepositoryFactoryBean;
import com.ssh.jpa.dao.user.IUserRoleDao;
import com.ssh.jpa.repository.user.IUserRoleRepository;

@Repository
public class UserRoleDao extends RepositoryFactoryBean<IUserRoleRepository> implements IUserRoleDao {
	
}
