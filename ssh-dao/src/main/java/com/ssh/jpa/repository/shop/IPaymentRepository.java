package com.ssh.jpa.repository.shop;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.shop.Payment;

public interface IPaymentRepository extends IBaseJpaRepository<Payment, Long> {

}
