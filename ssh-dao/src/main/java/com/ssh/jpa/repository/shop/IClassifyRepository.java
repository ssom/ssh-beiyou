package com.ssh.jpa.repository.shop;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.shop.Classify;

public interface IClassifyRepository extends IBaseJpaRepository<Classify, Long> {

}
