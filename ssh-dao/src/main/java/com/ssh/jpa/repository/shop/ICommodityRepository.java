package com.ssh.jpa.repository.shop;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.shop.Commodity;

public interface ICommodityRepository extends IBaseJpaRepository<Commodity, Long> {

}
