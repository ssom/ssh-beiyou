package com.ssh.jpa.repository.shop;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.shop.CommodityClassify;

public interface ICommodityClassifyRepository extends IBaseJpaRepository<CommodityClassify, Long> {

}
