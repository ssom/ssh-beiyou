package com.ssh.jpa.repository.user;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.user.PermissionResource;

public interface IPermissionResourceRepository extends IBaseJpaRepository<PermissionResource, Long> {

	@Modifying
	@Query(nativeQuery=true,value="delete from t_permission_resource where permissionId=:pId and resourceId=:rId")
	public void deleteByPidAndRid(@Param("pId") Long pId,@Param("rId") Long rId);
	
}
