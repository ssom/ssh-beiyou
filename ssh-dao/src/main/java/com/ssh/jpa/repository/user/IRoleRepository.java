package com.ssh.jpa.repository.user;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.user.Role;

public interface IRoleRepository extends IBaseJpaRepository<Role, Long> {

}
