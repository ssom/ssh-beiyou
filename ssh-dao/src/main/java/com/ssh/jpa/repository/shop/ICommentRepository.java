package com.ssh.jpa.repository.shop;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.shop.Comment;

public interface ICommentRepository extends IBaseJpaRepository<Comment, Long> {

}
