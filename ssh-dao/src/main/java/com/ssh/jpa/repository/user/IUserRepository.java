package com.ssh.jpa.repository.user;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.user.User;

public interface IUserRepository extends IBaseJpaRepository<User, Long> {
	
	public User findByUsername(String username);
	
}
