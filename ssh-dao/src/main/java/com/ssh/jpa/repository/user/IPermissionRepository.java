package com.ssh.jpa.repository.user;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.user.Permission;

public interface IPermissionRepository extends IBaseJpaRepository<Permission, Long> {

}
