package com.ssh.jpa.repository.user;

import org.springframework.data.jpa.repository.Modifying;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.user.RolePermission;

public interface IRolePermissionRepository extends IBaseJpaRepository<RolePermission, Long> {

	/**
	 * 根据角色id和权限id删除角色的权限
	 * @param roleId
	 * @param permissionId
	 */
	@Modifying
	public void deleteByRoleIdAndPermissionId(Long roleId,Long permissionId);
	
}
