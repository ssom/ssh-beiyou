package com.ssh.jpa.repository.shop;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.shop.OrderCommodity;

public interface IOrderCommodityRepository extends IBaseJpaRepository<OrderCommodity, Long> {

}
