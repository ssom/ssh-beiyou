package com.ssh.jpa.repository.user;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.user.UserRole;

public interface IUserRoleRepository extends IBaseJpaRepository<UserRole, Long> {

	public UserRole findByUserIdAndRoleId(Long userId,Long roleId);
	
	@Modifying
	@Query(nativeQuery=true,value="delete from t_user_role where userId=? and roleId=?")
	public Integer deleteByUserIdAndRoleId(Long userId,Long roleId);
	
}
