package com.ssh.jpa.repository.shop;

import java.util.List;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.shop.Order;

public interface IOrderRepository extends IBaseJpaRepository<Order, Long> {
	
	public List<Order> findById(Long id);

}
