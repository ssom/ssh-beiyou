package com.ssh.jpa.repository.user;

import com.ssh.common.jpa.IBaseJpaRepository;
import com.ssh.model.user.Resource;

public interface IResourceRepository extends IBaseJpaRepository<Resource, Long> {

}
