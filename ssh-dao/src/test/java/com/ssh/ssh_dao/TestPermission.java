package com.ssh.ssh_dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ssh.jpa.dao.user.IPermissionDao;
import com.ssh.jpa.dao.user.IRolePermissionDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/spring-persistence.xml")
public class TestPermission {
	
	@Autowired
	private IRolePermissionDao rolePermissionDao;
	@Autowired
	private IPermissionDao permissionDao;

	@Test
	public void testSelect() {
		rolePermissionDao.selectRolePermissions(23l);
	}
	
	@Test
	public void testSelectUserPermission() {
		List<String> lists = permissionDao.selectUserPermissions("孙悟空");
		for(String list:lists) {
			System.out.println(list);
		}
	}
	
}
