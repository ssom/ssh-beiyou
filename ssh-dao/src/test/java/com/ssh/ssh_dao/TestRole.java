package com.ssh.ssh_dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ssh.jpa.dao.user.IRoleDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/spring-persistence.xml")
public class TestRole {

	@Autowired
	private IRoleDao roleDao;
	
	@Test
	public void testUserRoles() {
		List<String> a = roleDao.selectUserRoles("孙悟空");
		System.out.println(a);
	}
	
}
