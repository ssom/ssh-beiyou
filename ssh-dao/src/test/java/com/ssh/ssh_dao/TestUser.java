package com.ssh.ssh_dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ssh.jpa.dao.user.IUserDao;
import com.ssh.jpa.dao.user.IUserRoleDao;
import com.ssh.jpa.repository.user.IUserRepository;
import com.ssh.model.user.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/spring-persistence.xml")
public class TestUser {
	
	@Autowired
	private IUserDao userDao;
	@Autowired
	private IUserRoleDao userRoleDao;
	@Autowired
	private IUserRepository userRepository;
	
	@Test
	public void testAdd() {
		User user = new User();
		user.setUsername("aaaa");
		user.setPassword("bbbb");
		userRepository.save(user);
	}
	
	@Test
	public void testQuery() {
		User map = userRepository.findOne((long)11);
		System.out.println(map);
	}
	
	@Test
	public void testFindById() {
		User user = userRepository.findByUsername("");
		System.out.println(user.getUsername());
	}
	
	@Test
	public void testSelectUserRoles() {
		/*List<Map<String,Object>> list = userRepository.selectUserRoles((long)19);
		String roles = JSON.toJSONString(list);
		System.out.println(roles);*/
		
	}
	
	@Test
	public void testApplicationContext() {
		User user = userDao.getRepositoryBean().findOne(30l);
		System.out.println(user.getName());
	}
	
	@Test
	public void testdelete() {
		int a = userRoleDao.getRepositoryBean().deleteByUserIdAndRoleId(19l, 23l);
		/*UserRole userRole = new UserRole();
		userRole.setRoleId(23l);
		userRole.setUserId(19l);
		userRoleDao.getRepositoryBean().delete(userRole);*/
	}
	
	@Test
	public void testFindByUsername() {
		User user = userRepository.findByUsername("aaaa");
		System.out.println(user.getPassword());
	}

}
