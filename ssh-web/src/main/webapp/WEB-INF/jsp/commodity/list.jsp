<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div>
		<table>
			<thead>
				<tr>
					<td>名字</td>
					<td>价格</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${commodities}" var="commoditie">
					<tr>
						<td>${commoditie.name}</td>
						<td>${commoditie.price}</td>
						<td>
							<a href="">加入购物车</a>
							<a href="<%=request.getContextPath()%>/order/add?commodityId=${commoditie.id}">购买</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>