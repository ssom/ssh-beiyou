/**
 * 用户名是否使用过
 */
(function ($) {
	$.fn.extend({
		isUse:function () {
			var that = this;
			that.mouseleave(function () {
				var username = $(that).val();
				var url = "/ssh-web/user/isUse?username="+username;
				$.ajax({
					url:url,
					//data: JSON.stringify(user),
					type:'post',
					async:true,
					dataType:'json',
					contentType:'application/json; charset=utf-8',
					success:function (data) {
						var hint = "<span class='hint'>"+data.errorMsg+"</span>";
						that.after(hint);
					}
				});
			});
			that.mouseenter(function () {
				$('.hint').remove();
			});
		}
	});
})(jQuery);