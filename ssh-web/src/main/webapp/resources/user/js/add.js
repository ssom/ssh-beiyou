/**
 * 添加功能
 */
(function ($) {
	$.fn.extend({
		add:function () {
			var that = this;
			that.click(function () {
				var username = $(that).siblings('.username').val();
				var password = $(that).siblings('.password').val();
				var user = {"username":username,"password":password};
				var url = "/ssh-web/user/add";
				$.ajax({
					url:url,
					data: JSON.stringify(user),
					type:'post',
					async:true,
					dataType:'json',
					contentType:'application/json; charset=utf-8',
					success:function (data) {
						console.log(data.data);
					}
				});
			});
		}
	});
})(jQuery);