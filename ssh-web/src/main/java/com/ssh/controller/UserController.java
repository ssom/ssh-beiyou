package com.ssh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ssh.common.BaseResponse;
import com.ssh.model.user.User;
import com.ssh.service.user.IUserService;

@RestController
@RequestMapping(value={"/user"})
public class UserController {
	
	@Autowired
	private IUserService userService;

	@RequestMapping(value={"/add"},method=RequestMethod.POST)
	public BaseResponse<Object> add(BaseResponse<Object> baseResponse,@RequestBody User user) {
		user = userService.add(user);
		baseResponse.setResult("1");
		baseResponse.setData(user);
		return baseResponse;
	}
	@RequestMapping(value={"/isUse"},method=RequestMethod.POST)
	public BaseResponse<Object> isUse(BaseResponse<Object> baseResponse,String username) {
		User user = userService.isUse(username);
		if(user == null) {
			baseResponse.setResult("1");
			baseResponse.setErrorMsg("该用户名可以使用!");
		} else {
			baseResponse.setResult("1");
			baseResponse.setErrorMsg("该用户名不可以使用!");
		}
		return baseResponse;
	}
	
	@RequestMapping(value={"/findById"},method=RequestMethod.POST)
	public BaseResponse<Object> findById(BaseResponse<Object> baseResponse,Long id) {
		User user = userService.findById(id);
		baseResponse.setResult("1");
		baseResponse.setData(user);
		return baseResponse;
	}
	
}
