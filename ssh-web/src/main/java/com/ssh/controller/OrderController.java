package com.ssh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ssh.model.shop.Order;
import com.ssh.service.shop.IOrderService;

@Controller
@RequestMapping(value = "/order")
public class OrderController {

	@Autowired
	private IOrderService orderService;

	/**
	 * 增加订单
	 * @param order			订单对象
	 * @param commodityId	商品id
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(Order order,Long commodityId) {
		orderService.add(order);
		return "redirect:/order/list";
	}

}
