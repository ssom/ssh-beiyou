package com.ssh.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ssh.model.shop.Commodity;
import com.ssh.model.user.User;
import com.ssh.service.shop.ICommodityService;
import com.ssh.service.user.IUserService;

@Controller
@RequestMapping(value="/commodity")
public class CommodityController {
	
	@Autowired
	private ICommodityService commodityService;
	@Autowired
	private IUserService userService;

	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(Model model) {
		List<Commodity> commodities = commodityService.list();
		User user = userService.findByUsername(username);
		model.addAttribute("commodities", commodities);
		return "commodity/list";
	}
	
}
