package com.ssh.common;

/**
 * 返回的数据对象
 * @author Cris
 *
 * @param <T>
 */
public class BaseResponse<T> {
	
	/**
	 * 请求是否出现异常【出现异常会返回0】
	 */
	private String result;
	/**
	 * 错误码
	 */
	private String errorCode;
	/**
	 * 错误信息
	 */
	private String errorMsg;
	/**
	 * 分页对象
	 */
	private Page page;
	/**
	 * 数据
	 */
	private T data;
	
	public BaseResponse() {
		super();
		this.errorCode = "";
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
}
