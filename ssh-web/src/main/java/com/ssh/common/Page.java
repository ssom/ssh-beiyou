package com.ssh.common;

import java.util.List;

/**
 * 分页对象
 * @author Cris
 *
 */
public class Page {

	/**
	 * 分页偏移量
	 */
	private Integer offset;
	/**
	 * 分页尺寸
	 */
	private Integer pagesize;
	/**
	 * 总页数
	 */
	private Integer total;
	/**
	 * 分页数据
	 */
	private List<?> data;

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getPagesize() {
		return pagesize;
	}

	public void setPagesize(Integer pagesize) {
		this.pagesize = pagesize;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}
}
