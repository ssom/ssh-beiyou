package com.ssh.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.ssh.model.user.User;

public class LoginInterceptor implements HandlerInterceptor {

	private List<String> path;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		String requestPath = request.getServletPath();
		String username = request.getParameter("username");
		if(username == null) {
			modelAndView.setViewName("/system/login");
		}
		if(path.equals(requestPath)) {
			if() {
				
			}
			User user = (User) request.getSession().getAttribute(username);
			modelAndView.setViewName("");
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

	public List<String> getPath() {
		return path;
	}

	public void setPath(List<String> path) {
		this.path = path;
	}

}
