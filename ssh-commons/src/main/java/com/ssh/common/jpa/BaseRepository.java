package com.ssh.common.jpa;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

public abstract class BaseRepository<T,ID extends Serializable> extends SimpleJpaRepository<T, ID> implements IBaseJpaRepository<T, ID> {

	public BaseRepository(Class<T> domainClass, EntityManager em) {
		super(domainClass, em);
	}

}
