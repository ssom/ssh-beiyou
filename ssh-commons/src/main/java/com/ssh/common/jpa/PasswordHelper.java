package com.ssh.common.jpa;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 * 
 * @author Cris
 *
 */
public class PasswordHelper {

    /**
     * 使用什么加密
     * @Value 可以获取到xxx.properties里面value值
     */
	//@Value("${password.algorithmName}")
    private static String algorithmName = "md5";
    
    /**
     * 加密迭代的次数
     */
	//@Value("${password.hashIterations}")
    private static int hashIterations = 2;
    
    /**
     * 用盐值加密后的新密码
     * @param password		密码
     * @param saltDensity	盐值
     * @return
     */
    public static String encryptPassword(String password,Object saltDensity) {
        String newPassword = new SimpleHash(
        		//使用的md5加密的
                algorithmName,
                //加密的对象
                password,
                //加密的盐值是使用的username
                genSaltDensity(saltDensity),
                //md5加密迭代的次数
                hashIterations).toHex();
        
        return newPassword;
    }
    
    /**
     * 生成一个字节盐值
     * @param saltDensity	盐值
     * @return
     */
    public static ByteSource genSaltDensity(Object saltDensity) {
    	return ByteSource.Util.bytes(saltDensity);
    }
    
}
