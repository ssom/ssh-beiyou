package com.ssh.common.jpa;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 获取子类的对象
 * @author Cris
 *
 * @param <T>
 */
@SuppressWarnings({"unchecked","static-access"})
public class RepositoryFactoryBean<T> implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext;

	@PersistenceContext
	protected EntityManager entityManager;

	public T getRepositoryBean() {
		return (T) applicationContext.getBean(this.getGenericType(0));
	}
	public T getRepositoryBean(Class<?> clazz) {
		return (T) applicationContext.getBean(clazz);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	/**
	 * 获取泛型的具体类型
	 * @param index
	 * @return
	 */
	private Class<?> getGenericType(int index) {
		Type genType = getClass().getGenericSuperclass();
		if (!(genType instanceof ParameterizedType)) {
			return Object.class;
		}
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		if (index >= params.length || index < 0) {
			throw new RuntimeException("Index outof bounds");
		}
		if (!(params[index] instanceof Class)) {
			return Object.class;
		}
		return (Class<?>) params[index];
	}
	
	/**
	 * 设置查询参数并且返回一个map集合
	 * @param query
	 * @param map		参数集合
	 * @return
	 */
	protected Query setParamAndMap(Query query,Map<String,Object> map) {
		Set<String> keys = map.keySet();
		for(String key:keys) {
			query.setParameter(key, map.get(key));
		}
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return query;
	}

}
