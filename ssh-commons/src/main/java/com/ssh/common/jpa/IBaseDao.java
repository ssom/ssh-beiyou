package com.ssh.common.jpa;

public interface IBaseDao<T> {

	public T getRepositoryBean();
	
}
