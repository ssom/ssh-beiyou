package com.ssh.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ssh.model.User;
import com.ssh.service.user.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/spring-business.xml")
public class TestUser {
	
	@Autowired
	private IUserService userService;
	
	@Test
	public void testUserAdd() {
		User user = new User();
		user.setUsername("xcc");
		user.setPassword("123");
		userService.add(user);
	}
	
}
