package com.ssh.service.shop;

import java.util.List;

import com.ssh.model.Commodity;

public interface ICommodityService {

	public Commodity add(Commodity commodity);
	
	public List<Commodity> list();
	
}
