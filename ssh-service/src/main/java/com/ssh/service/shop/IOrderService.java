package com.ssh.service.shop;

import java.util.List;

import com.ssh.model.Order;

public interface IOrderService {
	
	public Order add(Order order);
	/**
	 * 查询订单，如果是后台系统就需要查询所有的订单，如果是前台页面就要查看用户自己的订单了
	 * @param userId
	 * @return
	 */
	public List<Order> list(Long userId);

}
