package com.ssh.service.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssh.dao.OrderMapper;
import com.ssh.model.Order;
import com.ssh.service.shop.IOrderService;

@Service
public class OrderService implements IOrderService {
	
	@Autowired
	private OrderMapper orderMapper;

	@Override
	public Order add(Order order) {
		orderMapper.insertSelective(order);
		return order;
	}

	@Override
	public List<Order> list(Long userId) {
		List<Order> orders = null;
		if(userId == null || userId == 0) {
			orders = orderMapper.findAll();
		} else {
			orders = orderMapper.findByUserid(userId);
		}
		return orders;
	}

}
