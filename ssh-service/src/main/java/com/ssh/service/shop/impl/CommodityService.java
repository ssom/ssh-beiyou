package com.ssh.service.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssh.dao.CommodityMapper;
import com.ssh.model.Commodity;
import com.ssh.service.shop.ICommodityService;

@Service
public class CommodityService implements ICommodityService {

	@Autowired
	private CommodityMapper commodityMapper;

	@Override
	public Commodity add(Commodity commodity) {
		commodityMapper.insertSelective(commodity);
		return commodity;
	}

	@Override
	public List<Commodity> list() {
		return commodityMapper.findAll();
	}
	
}
