package com.ssh.service.user;

import java.util.List;
import java.util.Map;

import com.ssh.model.PermissionResource;

public interface IPermissionResourceService {

	public PermissionResource add(PermissionResource permissionResource);
	
	public void delete(PermissionResource permissionResource);
	/**
	 * 查询权限可以访问的所有的资源
	 * @param permissionId		权限id
	 * @return
	 */
	public List<Map<String,Object>> selectPermissionResources(Long permissionId);
	
}
