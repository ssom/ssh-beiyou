package com.ssh.service.user;

import java.util.List;

import com.ssh.model.Role;

public interface IRoleService {

	public Role add(Role role);
	/**
	 * 根据用户id查询用户
	 * @param id
	 * @return
	 */
	public Role findById(Long id);
	
	public List<Role> findAll();
	
	public Role updateRole(Role role);
	
	public void deleteRole(Long id);
	
	/**
	 * 根据用户名查询用户所有的角色
	 * @param username
	 * @return
	 */
	public List<String> selectUserRoles(String username);
	
}
