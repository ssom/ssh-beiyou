package com.ssh.service.user;

import java.util.List;

import com.ssh.model.Resource;

public interface IResourceService {

	public Resource add(Resource resource);
	/**
	 * 根据用户id查询用户
	 * @param id
	 * @return
	 */
	public Resource findById(Long id);
	
	public List<Resource> findAll();
	
	public Resource updateResource(Resource resource);
	
	public void deleteResource(Long id);
	
}
