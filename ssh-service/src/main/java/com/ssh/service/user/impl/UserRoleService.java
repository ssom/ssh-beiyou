package com.ssh.service.user.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssh.dao.UserRoleMapper;
import com.ssh.model.UserRole;
import com.ssh.service.user.IUserRoleService;

@Service
public class UserRoleService implements IUserRoleService {

	@Autowired
	private UserRoleMapper userRoleMapper;

	@Override
	public UserRole add(UserRole userRole) {
		userRole.setId(new Date().getTime());
		userRoleMapper.insertSelective(userRole);
		return userRole;
	}

	@Override
	public void delete(UserRole userRole) {
		userRoleMapper.deleteByUserIdAndRoleId(userRole.getUserid(), userRole.getRoleid());
	}

	@Override
	public List<Map<String, Object>> selectUserRoles(Long userId) {
		return userRoleMapper.selectUserRoles(userId);
	}
	
}
