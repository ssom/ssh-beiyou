package com.ssh.service.user.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssh.dao.PermissionResourceMapper;
import com.ssh.model.PermissionResource;
import com.ssh.service.user.IPermissionResourceService;

@Service
public class PermissionResourceService implements IPermissionResourceService {
	
	@Autowired
	private PermissionResourceMapper permissionResourceMapper;

	@Override
	public PermissionResource add(PermissionResource permissionResource) {
		permissionResource.setId(new Date().getTime());
		permissionResourceMapper.insertSelective(permissionResource);
		return permissionResource;
	}

	@Override
	public void delete(PermissionResource permissionResource) {
		permissionResourceMapper.deleteByPidAndRid(permissionResource.getPermissionid(), permissionResource.getResourceid());
	}

	@Override
	public List<Map<String, Object>> selectPermissionResources(Long permissionId) {
		return permissionResourceMapper.selectPermissionResources(permissionId);
	}

}
