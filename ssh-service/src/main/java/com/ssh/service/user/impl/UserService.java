package com.ssh.service.user.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.ssh.common.jpa.PasswordHelper;
import com.ssh.dao.UserMapper;
import com.ssh.model.User;
import com.ssh.service.user.IUserService;

@Service
public class UserService implements IUserService {

	@Autowired
	private UserMapper userMapper;
	
	@Override
	//@Cacheable(value="user",key="#id")
	public User add(User user) {
		user.setId(new Date().getTime());
		user.setPassword(PasswordHelper.encryptPassword(user.getPassword(), user.getUsername()));
		userMapper.insertSelective(user);
		return user;
	}

	@Override
	@Cacheable(value={"user"},key="#id")
	public User findById(Long id) {
		return userMapper.selectByPrimaryKey(id);
	}

	@Override
	@CachePut(value={"user"},key="#user.id")
	public User updateUser(User user) {
		int count = userMapper.insertSelective(user);
		return user;
	}

	@Override
	@CacheEvict(value={"user"},key="#id")
	public void deleteUser(Long id) {
		int count = userMapper.deleteByPrimaryKey(id);
	}

	@Override
	public User isUse(String userName) {
		return userMapper.selectByUsername(userName);
	}

	@Override
	public List<User> findAll() {
		return userMapper.selectAll();
	}

	@Override
	public User findByUsername(String username) {
		return userMapper.selectByUsername(username);
	}

	@Override
	public User login(String username, String password, String rememberMe) {
		User user = this.findByUsername(username);
		password = PasswordHelper.encryptPassword(password, username);
		if(user.getPassword().equals(password)) {
			return user;
		}
		return null;
	}

}
