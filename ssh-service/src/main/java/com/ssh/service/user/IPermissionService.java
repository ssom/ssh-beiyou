package com.ssh.service.user;

import java.util.List;

import com.ssh.model.Permission;

public interface IPermissionService {

	public Permission add(Permission permission);
	/**
	 * 根据用户id查询用户
	 * @param id
	 * @return
	 */
	public Permission findById(Long id);
	
	public List<Permission> findAll();
	
	public Permission updatePermission(Permission permission);
	
	public void deletePermission(Long id);
	/**
	 * 查询用户的所有的权限
	 * @param username		用户名
	 * @return
	 */
	public List<String> selectUserPermissions(String username);
	
}
