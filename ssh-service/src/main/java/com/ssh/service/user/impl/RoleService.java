package com.ssh.service.user.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssh.dao.RoleMapper;
import com.ssh.model.Role;
import com.ssh.service.user.IRoleService;

@Service
public class RoleService implements IRoleService {

	@Autowired
	private RoleMapper roleMapper;

	@Override
	public Role add(Role role) {
		role.setId(new Date().getTime());
		roleMapper.insertSelective(role);
		return role;
	}

	@Override
	public Role findById(Long id) {
		return roleMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Role> findAll() {
		return roleMapper.selectByAll();
	}

	@Override
	public Role updateRole(Role role) {
		roleMapper.updateByPrimaryKeySelective(role);
		return role;
	}

	@Override
	public void deleteRole(Long id) {
		roleMapper.deleteByPrimaryKey(id);
	}

	@Override
	public List<String> selectUserRoles(String username) {
		return roleMapper.selectByUsername(username);
	}
	
}
