package com.ssh.service.user;

import java.util.List;
import java.util.Map;

import com.ssh.model.UserRole;

public interface IUserRoleService {

	public UserRole add(UserRole userRole);
	
	/**
	 * 
	 * @param userRoleId
	 * @return
	 */
	public void delete(UserRole userRole);
	
	public List<Map<String,Object>> selectUserRoles(Long userId);
	
}
