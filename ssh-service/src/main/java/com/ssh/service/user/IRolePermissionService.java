package com.ssh.service.user;

import java.util.List;
import java.util.Map;

import com.ssh.model.RolePermission;

public interface IRolePermissionService {

	public RolePermission add(RolePermission rolePermission);
	
	public void delete(RolePermission rolePermission);
	/**
	 * 查询角色的所有权限
	 * @param roleId		角色id
	 * @return
	 */
	public List<Map<String,Object>> selectRolePermissions(Long roleId);
	
}
