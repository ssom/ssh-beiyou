package com.ssh.service.user.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssh.dao.PermissionMapper;
import com.ssh.model.Permission;
import com.ssh.service.user.IPermissionService;

@Service
public class PermissionService implements IPermissionService {
	
	@Autowired
	private PermissionMapper permissionMapper;

	@Override
	public Permission add(Permission permission) {
		permission.setId(new Date().getTime());
		permissionMapper.insertSelective(permission);
		return permission;
	}

	@Override
	public Permission findById(Long id) {
		return permissionMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Permission> findAll() {
		return permissionMapper.findAll();
	}

	@Override
	public Permission updatePermission(Permission permission) {
		permissionMapper.updateByPrimaryKeySelective(permission);
		return permission;
	}

	@Override
	public void deletePermission(Long id) {
		permissionMapper.deleteByPrimaryKey(id);
	}

	@Override
	public List<String> selectUserPermissions(String username) {
		return permissionMapper.selectUserPermissions(username);
	}

}
