package com.ssh.service.user.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssh.dao.RolePermissionMapper;
import com.ssh.model.RolePermission;
import com.ssh.service.user.IRolePermissionService;

@Service
public class RolePermissionService implements IRolePermissionService {
	
	@Autowired
	private RolePermissionMapper rolePermissionMapper;

	@Override
	public RolePermission add(RolePermission rolePermission) {
		rolePermission.setId(new Date().getTime());
		rolePermissionMapper.insertSelective(rolePermission);
		return rolePermission;
	}

	@Override
	public void delete(RolePermission rolePermission) {
		rolePermissionMapper.deleteByRoleIdAndPermissionId(rolePermission.getRoleid(), rolePermission.getPermissionid());
	}

	@Override
	public List<Map<String, Object>> selectRolePermissions(Long roleId) {
		return rolePermissionMapper.selectRolePermissions(roleId);
	}

}
