package com.ssh.service.user;

import java.util.List;

import com.ssh.model.User;

public interface IUserService {
	
	public User add(User user);
	/**
	 * 是否使用了该用户名
	 * @param userName
	 * @return
	 */
	public User isUse(String userName);
	/**
	 * 根据用户id查询用户
	 * @param id
	 * @return
	 */
	public User findById(Long id);
	
	public List<User> findAll();
	
	public User updateUser(User user);
	
	public void deleteUser(Long id);
	
	public User findByUsername(String username);
	
	public User login(String username,String password,String rememberMe);
}
