package com.ssh.service.user.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssh.dao.ResourceMapper;
import com.ssh.model.Resource;
import com.ssh.service.user.IResourceService;

@Service
public class ResourceService implements IResourceService {
	
	@Autowired
	private ResourceMapper resourceMapper;

	@Override
	public Resource add(Resource resource) {
		resource.setId(new Date().getTime());
		resourceMapper.insertSelective(resource);
		return resource;
	}

	@Override
	public Resource findById(Long id) {
		return resourceMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Resource> findAll() {
		return resourceMapper.findAll();
	}

	@Override
	public Resource updateResource(Resource resource) {
		resourceMapper.updateByPrimaryKeySelective(resource);
		return resource;
	}

	@Override
	public void deleteResource(Long id) {
		resourceMapper.deleteByPrimaryKey(id);
	}

}
