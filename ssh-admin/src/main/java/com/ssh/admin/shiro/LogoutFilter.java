package com.ssh.admin.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.web.filter.PathMatchingFilter;

public class LogoutFilter extends PathMatchingFilter {

	@Override
	protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue)
			throws Exception {
		//使用权限管理工具进行用户的退出，跳出登陆，给出提示信息
		SecurityUtils.getSubject().logout();
		return super.onPreHandle(request, response, mappedValue);
	}

}
