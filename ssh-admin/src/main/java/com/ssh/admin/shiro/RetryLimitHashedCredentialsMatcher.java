package com.ssh.admin.shiro;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

/**
 * 自己实现登陆认证，因为我们使用了base64进行了加密，所以需要实现这个【散列凭证匹配器】
 * @author SSOM
 *
 */
public class RetryLimitHashedCredentialsMatcher extends HashedCredentialsMatcher {

	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
		token.getPrincipal();
		token.getCredentials();
		return super.doCredentialsMatch(token, info);
	}

}
