package com.ssh.admin.shiro;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.ssh.common.jpa.PasswordHelper;
import com.ssh.model.User;
import com.ssh.service.user.IPermissionService;
import com.ssh.service.user.IRoleService;
import com.ssh.service.user.IUserService;

/**
 * 用户的授权【改类已经被spring所管理，可以使用依赖注入】
 * 这是shiro最先进入的类，应该是最开始的拦截器
 * @author SSOM
 *
 */
public class UserRealm extends AuthorizingRealm {
	
	@Autowired
	private IUserService userService;
	@Autowired
	private IRoleService roleService;
	@Autowired
	private IPermissionService permissionService;
	
	/**
	 * 该方法是做权限认证的【只要在shiro配置里面加上】
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		//获取一个用户
		String username = (String)principals.getPrimaryPrincipal();
		//授权
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		//设置角色
		Set<String> setRoles = new HashSet<>();
		setRoles.addAll(roleService.selectUserRoles(username));
		authorizationInfo.setRoles(setRoles);
		//设置权限
		Set<String> setPermissions = new HashSet<>();
		setPermissions.addAll(permissionService.selectUserPermissions(username));
		authorizationInfo.setStringPermissions(setPermissions);
		
		return authorizationInfo;
	}
	
	/**
	 * 该方法通过aop切入进来的
	 * 该方法是做登录认证的【只有过了该方法，才会进入spring的controller里面，这个方法里面已经实现了用的登录】
	 * 【只要在shiro配置里面加上authc都会做登录认证的】
	 * 【如果用户登录成功之后，那么就不会再进入该方法】
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) {
		String username = token.getPrincipal().toString();
		
		User user = userService.findByUsername(username);
		//没找到帐号
		if(user == null) throw new UnknownAccountException();
		
		//交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以在此判断或自定义实现
		SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo();
		//设置用户名
		authenticationInfo.setPrincipals( new SimplePrincipalCollection(user.getUsername(), getName()) );
		//设置密码
		authenticationInfo.setCredentials(user.getPassword());
		//设置盐值【是为了进行hash运算的时候，提供的一个值】
		authenticationInfo.setCredentialsSalt(PasswordHelper.genSaltDensity(user.getUsername()));
		return authenticationInfo;
	}
	
}
