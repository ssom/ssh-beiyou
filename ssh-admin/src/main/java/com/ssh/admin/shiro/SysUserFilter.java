package com.ssh.admin.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.springframework.beans.factory.annotation.Autowired;

import com.ssh.service.user.IUserService;

/**
 * 当用户登录成功之后，每次的请求都会进入该方法里面
 * @author SSOM
 *
 */
public class SysUserFilter extends PathMatchingFilter {

	@Autowired
    private IUserService userService;
    
    /**
     * 当用户登录成功之后，每一个请求该方法都会执行，它的目的就是把用户的信息存储到servlet的request里面，
     * 这样每次有请求，shiro都会从SecurityManager里面得到Subject，再通过Subject得到主体【即用户】
     * 
     * 这和把用户的信息存储到session里面并没有多大的区别，
     * 
     * 该方法在父类中没有具体的实现，下面的实现是自己写的
     */
    @Override
    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
    	//从shiro主体中获取一个用户名
        String username = (String)SecurityUtils.getSubject().getPrincipal();
        //把主体用户放在request中
        request.setAttribute("user", userService.findByUsername(username));
        return true;
    }
}
