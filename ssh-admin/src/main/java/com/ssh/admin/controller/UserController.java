package com.ssh.admin.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ssh.model.User;
import com.ssh.model.UserRole;
import com.ssh.service.user.IUserRoleService;
import com.ssh.service.user.IUserService;

@Controller
@RequestMapping(value="/user")
public class UserController {
	
	@Autowired
	private IUserService userService;
	@Autowired
	private IUserRoleService userRoleService;

	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(Model model) {
		return "user/add";
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String add(User user) {
		userService.add(user);
		return "redirect:/user/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public String delete(Long id) {
		userService.deleteUser(id);
		return "redirect:/user/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/update",method=RequestMethod.GET)
	public String update(Long id,Model model) {
		User user = userService.findById(id);
		model.addAttribute("user", user);
		return "user/update";
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public String update(User user,RedirectAttributes attributes) {
		user = userService.add(user);
		attributes.addFlashAttribute("userId", user.getId());
		return "redirect:/user/detail?id="+user.getId();
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/detail",method=RequestMethod.GET)
	public String detail(Long id,Model model) {
		User user = userService.findById(id);
		model.addAttribute("user", user);
		return "user/detail";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(Model model) {
		List<User> users = userService.findAll();
		model.addAttribute("users", users);
		return "user/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/selectUserRoles",method=RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> selectUserRoles(Long userId) {
		return userRoleService.selectUserRoles(userId);
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/updateRoles",method=RequestMethod.GET)
	public String updateRoles(Long userId,Model model) {
		List<Map<String,Object>> roles = userRoleService.selectUserRoles(userId);
		User user = userService.findById(userId);
		model.addAttribute("roles", roles);
		model.addAttribute("user", user);
		return "user/updateRoles";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/addUserRole",method=RequestMethod.POST)
	public @ResponseBody UserRole addUserRole(UserRole userRole) {
		return userRoleService.add(userRole);
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/deleteUserRole",method=RequestMethod.POST)
	public @ResponseBody String deleteUserRole(UserRole userRole) {
		userRoleService.delete(userRole);
		return "";
	}
	
}
