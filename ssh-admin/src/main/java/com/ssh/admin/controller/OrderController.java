package com.ssh.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/order")
public class OrderController {

	@RequestMapping(value="/query",method=RequestMethod.GET)
	public String query(Model model) {
		model.addAttribute("orders", "");
		return "order/query";
	}
	@RequestMapping(value="/query",method=RequestMethod.POST)
	public String query() {
		return "order/query";
	}
	
}
