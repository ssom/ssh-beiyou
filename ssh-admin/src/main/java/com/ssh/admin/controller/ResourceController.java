package com.ssh.admin.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ssh.model.Resource;
import com.ssh.service.user.IResourceService;

@Controller
@RequestMapping(value="/resource")
public class ResourceController {

	@Autowired
	private IResourceService resourceService;

	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(Model model) {
		return "resource/add";
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String add(Resource resource) {
		resourceService.add(resource);
		return "redirect:/resource/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public String delete(Long id) {
		resourceService.deleteResource(id);
		return "redirect:/resource/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/update",method=RequestMethod.GET)
	public String update(Long id,Model model) {
		Resource resource = resourceService.findById(id);
		model.addAttribute("resource", resource);
		return "resource/update";
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public String update(Resource resource,RedirectAttributes attributes) {
		resource = resourceService.add(resource);
		attributes.addFlashAttribute("userId", resource.getId());
		return "redirect:/resource/detail?id="+resource.getId();
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/detail",method=RequestMethod.GET)
	public String detail(Long id,Model model) {
		Resource resource = resourceService.findById(id);
		model.addAttribute("resource", resource);
		return "resource/detail";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(Model model) {
		List<Resource> resources = resourceService.findAll();
		model.addAttribute("resources", resources);
		return "resource/list";
	}
	
}
