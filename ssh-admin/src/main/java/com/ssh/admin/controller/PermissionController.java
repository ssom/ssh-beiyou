package com.ssh.admin.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ssh.model.Permission;
import com.ssh.model.PermissionResource;
import com.ssh.service.user.IPermissionResourceService;
import com.ssh.service.user.IPermissionService;

@Controller
@RequestMapping(value="/permission")
public class PermissionController {
	
	@Autowired
	private IPermissionService permissionService;
	@Autowired
	private IPermissionResourceService permissionResourceService;

	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(Model model) {
		return "permission/add";
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String add(Permission permission) {
		permissionService.add(permission);
		return "redirect:/permission/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public String delete(Long id) {
		permissionService.deletePermission(id);
		return "redirect:/permission/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/update",method=RequestMethod.GET)
	public String update(Long id,Model model) {
		Permission permission = permissionService.findById(id);
		model.addAttribute("permission", permission);
		return "permission/update";
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public String update(Permission permission,RedirectAttributes attributes) {
		permission = permissionService.add(permission);
		attributes.addFlashAttribute("permissionId", permission.getId());
		return "redirect:/permission/detail?id="+permission.getId();
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/detail",method=RequestMethod.GET)
	public String detail(Long id,Model model) {
		Permission permission = permissionService.findById(id);
		model.addAttribute("permission", permission);
		return "permission/detail";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(Model model) {
		List<Permission> permissions = permissionService.findAll();
		model.addAttribute("permissions", permissions);
		return "permission/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/updateResources",method=RequestMethod.GET)
	public String updateResources(Long permissionId,Model model) {
		List<Map<String,Object>> permissionResources = permissionResourceService.selectPermissionResources(permissionId);
		Permission permission = permissionService.findById(permissionId);
		model.addAttribute("permissionResources", permissionResources);
		model.addAttribute("permission", permission);
		return "permission/updateResources";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/addPermissionResource",method=RequestMethod.POST)
	public @ResponseBody PermissionResource addPermissionResource(PermissionResource permissionResource) {
		return permissionResourceService.add(permissionResource);
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/deletePermissionResource",method=RequestMethod.POST)
	public @ResponseBody String deletePermissionResource(PermissionResource permissionResource) {
		permissionResourceService.delete(permissionResource);
		return "";
	}
	
}
