package com.ssh.admin.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ssh.model.Role;
import com.ssh.model.RolePermission;
import com.ssh.service.user.IRolePermissionService;
import com.ssh.service.user.IRoleService;

@Controller
@RequestMapping(value="/role")
public class RoleController {
	
	@Autowired
	private IRoleService roleService;
	@Autowired
	private IRolePermissionService rolePermissionService;

	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(Model model) {
		return "role/add";
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String add(Role role) {
		roleService.add(role);
		return "redirect:/role/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public String delete(Long id) {
		roleService.deleteRole(id);
		return "redirect:/role/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/update",method=RequestMethod.GET)
	public String update(Long id,Model model) {
		Role role = roleService.findById(id);
		model.addAttribute("role", role);
		return "role/update";
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public String update(Role role,RedirectAttributes attributes) {
		role = roleService.add(role);
		attributes.addFlashAttribute("roleId", role.getId());
		return "redirect:/role/detail?id="+role.getId();
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/detail",method=RequestMethod.GET)
	public String detail(Long id,Model model) {
		Role role = roleService.findById(id);
		model.addAttribute("role", role);
		return "role/detail";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(Model model) {
		List<Role> roles = roleService.findAll();
		model.addAttribute("roles", roles);
		return "role/list";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/updatePermissions",method=RequestMethod.GET)
	public String updatePermissions(Long roleId,Model model) {
		List<Map<String,Object>> permissions = rolePermissionService.selectRolePermissions(roleId);
		Role role = roleService.findById(roleId);
		model.addAttribute("permissions", permissions);
		model.addAttribute("role", role);
		return "role/updatePermissions";
	}
	
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/addRolePermission",method=RequestMethod.POST)
	public @ResponseBody RolePermission addUserRole(RolePermission rolePermission) {
		return rolePermissionService.add(rolePermission);
	}
	@RequiresRoles(value={"admin"})
	@RequestMapping(value="/deleteRolePermission",method=RequestMethod.POST)
	public @ResponseBody String deleteUserRole(RolePermission rolePermission) {
		rolePermissionService.delete(rolePermission);
		return "";
	}
	
}
