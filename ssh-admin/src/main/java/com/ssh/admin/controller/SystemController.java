package com.ssh.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ssh.model.User;
import com.ssh.service.user.IUserService;

@Controller
@RequestMapping(value="/system")
public class SystemController {
	
	@Autowired
	private IUserService userService;

	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login() {
		return "system/login";
	}
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(String username,String password,String rememberMe,HttpServletRequest request) {
		User user = userService.login(username, password, rememberMe);
		
		String exceptionClassName = (String)request.getAttribute("shiroLoginFailure");
        String error = null;
        if(UnknownAccountException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误";
        } else if(IncorrectCredentialsException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误";
        } else if(exceptionClassName != null) {
            error = "其他错误：" + exceptionClassName;
        }
		
		if(user == null) {
			return "system/login";
		}
		return "redirect:/";
	}
	
	/**
	 * 退出系统
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public String logout(RedirectAttributes redirectAttributes) {
		redirectAttributes.addFlashAttribute("message","您已安全退出");
		return "redirect:/system/login";
	}
	
	@RequestMapping(value="/403")
	public String unauthorizedRole() {
		return "system/403";
	}
	
}
