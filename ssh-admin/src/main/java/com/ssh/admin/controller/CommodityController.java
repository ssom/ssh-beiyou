package com.ssh.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ssh.model.Commodity;
import com.ssh.service.shop.ICommodityService;

@Controller
@RequestMapping(value="/commodity")
public class CommodityController {
	
	@Autowired
	private ICommodityService commodityService;

	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(Model model) {
		return "commodity/add";
	}
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String add(Commodity commodity) {
		//commodityService.add(commodity);
		return "redirect:/commodity/list";
	}
	
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public String delete(Long id) {
		return "redirect:/commodity/list";
	}
	
	@RequestMapping(value="/update",method=RequestMethod.GET)
	public String update(Model model) {
		return "commodity/update";
	}
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public String update(Commodity commodity) {
		return "redirect:/commodity/list";
	}
	
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(Model model) {
		//List<Commodity> commodities = commodityService.list();
		//model.addAttribute("commoditys", commodities);
		return "commodity/list";
	}
	
}
