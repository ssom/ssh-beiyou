<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div>
		<shiro:hasRole name="admin">
			<div>
				<ul>
					<li><h3>用户操作</h3></li>
					<li><a href="<%=request.getContextPath()%>/user/add" target="content">添加</a><br></li>
					<li><a href="<%=request.getContextPath()%>/user/list" target="content">列表</a></li>
				</ul>
				
				<ul>
					<li><h3>角色操作</h3></li>
					<li><a href="<%=request.getContextPath()%>/role/add" target="content">添加</a><br></li>
					<li><a href="<%=request.getContextPath()%>/role/list" target="content">列表</a></li>
				</ul>
				
				<ul>
					<li><h3>权限操作</h3></li>
					<li><a href="<%=request.getContextPath()%>/permission/add" target="content">添加</a><br></li>
					<li><a href="<%=request.getContextPath()%>/permission/list" target="content">列表</a></li>
				</ul>
				
				<ul>
					<li><h3>资源操作</h3></li>
					<li><a href="<%=request.getContextPath()%>/resource/add" target="content">添加</a><br></li>
					<li><a href="<%=request.getContextPath()%>/resource/list" target="content">列表</a></li>
				</ul>
			</div>
		</shiro:hasRole>
		
		<div>
			<ul>
				<li><h3>商品</h3></li>
				<li><a href="<%=request.getContextPath()%>/commodity/add" target="content">添加</a></li>
				<li><a href="<%=request.getContextPath()%>/commodity/list" target="content">列表</a></li>
			</ul>
			
			<ul>
				<li><h3>订单</h3></li>
				<li><a target="content">添加</a></li>
				<li><a target="content">列表</a></li>
			</ul>
		</div>
		
		<div>
		</div>
	</div>
</body>
</html>