/**
 * 
 */
(function ($) {
	$.fn.extend({
		updateRole:function () {
			$(this).on('click',function (e) {
				var checked = $(this).is(":checked");
				var userId = $(this).siblings(".user-id").val();
				var roleId = $(this).siblings(".role-id").val();
				if(checked) {
					addUserRole(userId,roleId);
				} else {
					deleteUserRole(userId,roleId);
				}
			});
		}
	});
	
	/**
	 * 添加用户角色
	 * @returns
	 */
	function addUserRole(userId,roleId) {
		//var userRole = {"userId":userId,"roleId":roleId};
		var url = "/ssh-admin/user/addUserRole?userid="+userId+"&roleid="+roleId;
		$.ajax({
			url:url,
			//data: JSON.stringify(userRole),
			type:'post',
			async:true,
			dataType:'json',
			contentType:'application/json; charset=utf-8',
			success:function (datas) {
			}
		});
	}
	
	/**
	 * 删除用户角色
	 * @returns
	 */
	function deleteUserRole(userId,roleId) {
		var url = "/ssh-admin/user/deleteUserRole?userid="+userId+"&roleid="+roleId;
		$.ajax({
			url:url,
			//data: JSON.stringify(userRole),
			type:'post',
			async:true,
			dataType:'json',
			contentType:'application/json; charset=utf-8',
			success:function (datas) {
			}
		});
	}
})(jQuery)