/**
 * 
 */
(function ($) {
	$.fn.extend({
		updatePermission:function () {
			$(this).on('click',function (e) {
				var checked = $(this).is(":checked");
				var roleId = $(this).siblings(".role-id").val();
				var permissionId = $(this).siblings(".permission-id").val();
				if(checked) {
					addRolePermission(roleId,permissionId);
				} else {
					deleteRolePermission(roleId,permissionId);
				}
			});
		}
	});
	
	/**
	 * 删除角色权限
	 * @returns
	 */
	function addRolePermission(roleId,permissionId) {
		//var userRole = {"userId":userId,"roleId":roleId};
		var url = "/ssh-admin/role/addRolePermission?roleId="+roleId+"&permissionId="+permissionId;
		$.ajax({
			url:url,
			//data: JSON.stringify(userRole),
			type:'post',
			async:true,
			dataType:'json',
			contentType:'application/json; charset=utf-8',
			success:function (datas) {
			}
		});
	}
	
	/**
	 * 添加角色权限
	 * @returns
	 */
	function deleteRolePermission(roleId,permissionId) {
		var url = "/ssh-admin/role/deleteRolePermission?roleId="+roleId+"&permissionId="+permissionId;
		$.ajax({
			url:url,
			//data: JSON.stringify(userRole),
			type:'post',
			async:true,
			dataType:'json',
			contentType:'application/json; charset=utf-8',
			success:function (datas) {
			}
		});
	}
})(jQuery)