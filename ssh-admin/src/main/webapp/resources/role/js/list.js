/**
 * 添加功能
 */
(function ($) {
	$.fn.extend({
		layer:function () {
			var that = this;
			
			var w,h,className;
			function getSrceenWH(){
				w = $(window).width();
				h = $(window).height();
				$('#dialogBg').width(w).height(h);
			}

			window.onresize = function(){  
				getSrceenWH();
			}  
			$(window).resize();  

			$(function(){
				getSrceenWH();
				
				//显示弹框
				$('input.rollIn').click(function(){
					var userId = $(this).parent('td').siblings('.user-id').children('a').text();
					var datas = selectUserRoles(userId);
					for(var data in datas) {
						var html = "<li class='drop'>"+
							"<label>"+
								"<font color='#ff0000'>* </font>"+datas[data][1]+
								"<input type='checkbox' name='roleIds' value='"+datas[data][0]+"'/>"+
								"<input type='text' name='userId' value='"+userId+"'/>"+
							"</label>"+
						"</li>";
						//给选中的标签追加子标签
						$('ul.editInfos').prepend(html);
						
						if(datas[data][2]==1) {
							alert("aa");
							$('input.ipt').attr("checked",true); 
						}
					}
						
					className = $(this).attr('class');
					$('#dialogBg').fadeIn(300);
					$('#dialog').removeAttr('class').addClass('animated '+className+'').fadeIn();
				});
				
				//关闭弹窗
				$('.claseDialogBtn').click(function(){
					$('#dialogBg').fadeOut(300,function(){
						$('#dialog').addClass('bounceOutUp').fadeOut();
						$('.drop').addClass('bounceOutUp').fadeOut().fadeOut().remove();
					});
				});
			});
		}
	});
	
	/**
	 * 查询用户的所有角色
	 */
	function selectUserRoles(userId) {
		var result = null;
		var url = "/ssh-admin/user/selectUserRoles?userId="+userId;
		$.ajax({
			url:url,
			//data: JSON.stringify(user),
			type:'get',
			async:false,
			dataType:'json',
			contentType:'application/json; charset=utf-8',
			success:function (datas) {
				for(var data in datas) {
					console.log(data);
				}
				result = datas;
			}
		});
		return result;
	}
})(jQuery);