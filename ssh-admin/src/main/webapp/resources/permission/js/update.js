/**
 * 
 */
(function ($) {
	$.fn.extend({
		updatePermissionResource:function () {
			$(this).on('click',function (e) {
				var checked = $(this).is(":checked");
				var permissionId = $(this).siblings(".permission-id").val();
				var resourceId = $(this).siblings(".resource-id").val();
				if(checked) {
					addPermissionResource(permissionId,resourceId);
				} else {
					deletePermissionResource(permissionId,resourceId);
				}
			});
		}
	});
	
	/**
	 * 删除角色权限
	 * @returns
	 */
	function addPermissionResource(permissionId,resourceId) {
		//var userRole = {"userId":userId,"roleId":roleId};
		var url = "/ssh-admin/permission/addPermissionResource?permissionId="+permissionId+"&resourceId="+resourceId;
		$.ajax({
			url:url,
			//data: JSON.stringify(userRole),
			type:'post',
			async:true,
			dataType:'json',
			contentType:'application/json; charset=utf-8',
			success:function (datas) {
			}
		});
	}
	
	/**
	 * 添加角色权限
	 * @returns
	 */
	function deletePermissionResource(permissionId,resourceId) {
		var url = "/ssh-admin/permission/deletePermissionResource?permissionId="+permissionId+"&resourceId="+resourceId;
		$.ajax({
			url:url,
			//data: JSON.stringify(userRole),
			type:'post',
			async:true,
			dataType:'json',
			contentType:'application/json; charset=utf-8',
			success:function (datas) {
			}
		});
	}
})(jQuery)