<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<a>列表</a>
	<div>
		<table>
			<thead>
				<tr>
					<td>id</td>
					<td>name</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${roles}" var="role">
					<tr>
						<td>
							<a href="<%=request.getContextPath()%>/role/detail?id=${role.id}">${role.id}</a>
						</td>
						<td>${role.name}</td>
						<td>
							<a href="<%=request.getContextPath()%>/role/update?id=${role.id}">修改</a>
							<a href="<%=request.getContextPath()%>/role/delete?id=${role.id}">删除</a>
							<a href="<%=request.getContextPath()%>/role/updatePermissions?roleId=${role.id}">指定角色的权限</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>.
	</div>
</body>
</html>