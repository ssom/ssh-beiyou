<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<script type="text/javascript" src="<%=request.getContextPath()%>/resources/lib/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/resources/role/js/update.js"></script>
	<script type="text/javascript">
		$(function () {
			$(".checkbox").updatePermission();
		});
	</script>
</head>
<body>
	<div>
		<h2>更新角色所对应的权限</h2>
	</div>
	<div>
		<form action="" method="post">
			<c:forEach items="${permissions}" var="permission">
				<div>
					<label>${permission.name}</label>
					<c:choose>
						<c:when test="${permission.isCheck == 1}">
							<input type="checkbox" class="checkbox" checked="checked">
						</c:when>
						<c:when test="${permission.isCheck == 0}">
							<input type="checkbox" class="checkbox">
						</c:when>
					</c:choose>
					<label>角色id：</label><input type="text" class="role-id" value="${role.id}">
					<label>权限id：</label><input type="text" class="permission-id" value="${permission.permissionId}">
				</div>
			</c:forEach>
		</form>
	</div>
	
</body>
</html>