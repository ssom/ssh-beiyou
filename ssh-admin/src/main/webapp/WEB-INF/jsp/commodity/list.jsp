<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>

<body>
	<div>
		<h2>商品列表</h2>
	</div>
	<div>
		<table>
			<thead>
				<tr>
					<td>名字</td>
					<td>库存数</td>
					<td>产地</td>
					<td>价格</td>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach items="${commoditys}" var="commodity">
					<tr>
						<td>${commodity.name}</td>
						<td>${commodity.number}</td>
						<td>${commodity.origin}</td>
						<td>${commodity.price}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>
</body>
	
</html>