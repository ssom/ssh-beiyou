<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<script type="text/javascript" src="<%=request.getContextPath()%>/resources/lib/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/resources/user/js/updateRoles.js"></script>
	<script type="text/javascript">
		$(function () {
			$(".checkbox").updateRole();
		});
	</script>
</head>
<body>
	<div>
		<h2>更新用户所对应的角色</h2>
	</div>
	<div>
		<span>${user.username}</span><br>
		<span>${user.name}</span><br>
		<form action="<%=request.getContextPath()%>/user/updateRoles" method="post">
			<c:forEach items="${roles}" var="role">
				<div>
					<label>${role.name}</label>
					<c:choose>
						<c:when test="${role.isCheck == 1}">
							<input type="checkbox" class="checkbox" checked="checked">
						</c:when>
						<c:when test="${role.isCheck == 0}">
							<input type="checkbox" class="checkbox">
						</c:when>
					</c:choose>
					<input type="text" class="user-id" value="${user.id}">
					<input type="text" class="role-id" value="${role.roleId}">
				</div>
			</c:forEach>
		</form>
	</div>
	
</body>
</html>