<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">  
	<meta name="apple-mobile-web-app-status-bar-style" content="black">  
	<meta content="telephone=no" name="format-detection">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/lib/jQuery-tck/css/common.css"/>
	
	<title>Insert title here</title>
	<script type="text/javascript" src="<%=request.getContextPath()%>/resources/lib/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/resources/user/js/list.js"></script>
	<script type="text/javascript">
		$(function () {
			//$('.add-role').addRole();
			$('.add-role').layer();
		});
	</script>
</head>
<body>
	<a>列表</a>
	<div>
		<table>
			<thead>
				<tr>
					<td>id</td>
					<td>username</td>
					<td>name</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${users}" var="user">
					<tr>
						<td class="user-id">
							<a href="<%=request.getContextPath()%>/user/detail?id=${user.id}">${user.id}</a>
						</td>
						<td>${user.username}</td>
						<td>${user.name}</td>
						<td>
							<a href="<%=request.getContextPath()%>/user/update?id=${user.id}">修改</a>
							<a href="<%=request.getContextPath()%>/user/delete?id=${user.id}">删除</a>
							<a href="<%=request.getContextPath()%>/user/updateRoles?userId=${user.id}">指定用户的角色</a>
							<%-- <input class="add-role rollIn" type="button" value="">
							<input class="user-id" type="hidden" value="${user.id}"> --%>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
	<!-- 弹层对话框 -->
	<div>
		<div id="wrapper">
			<div class="box">
				<div id="dialogBg"></div>
				<div id="dialog" class="animated">
					<img class="dialogIco" width="50" height="50" src="<%=request.getContextPath()%>/resources/lib/jQuery-tck/images/ico.png" alt="" />
					<div class="dialogTop">
						<a href="javascript:;" class="claseDialogBtn">关闭</a>
					</div>
					<form action="<%=request.getContextPath()%>/user/addRoles" method="post" id="editForm">
						<ul class="editInfos">
							<li><input type="submit" value="确认提交" class="submitBtn" /></li>
						</ul>
					</form>
				</div>
			</div>
			
		</div>
		<div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
		</div>
	</div>
	
	aaaaaaa<input type="checkbox" name="sdf" value="vvv">
</body>
</html>