<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<script type="text/javascript" src="<%=request.getContextPath()%>/resources/lib/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/resources/permission/js/update.js"></script>
	<script type="text/javascript">
		$(function () {
			$(".checkbox").updatePermissionResource();
		});
	</script>
</head>
<body>
	<div>
		<h2>更新权限所能访问到的资源</h2>
	</div>
	<div>
		<form action="<%=request.getContextPath()%>/permission/updatePermissions" method="post">
			<c:forEach items="${permissionResources}" var="pr">
				<div>
					<label style="width:50px;">${pr.name}</label>
					<c:choose>
						<c:when test="${pr.isCheck == 1}">
							<input type="checkbox" class="checkbox" checked="checked">
						</c:when>
						<c:when test="${pr.isCheck == 0}">
							<input type="checkbox" class="checkbox">
						</c:when>
					</c:choose>
					<label>资源id:</label><input type="text" class="resource-id" value="${pr.rId}">
					<label>权限资源id:</label><input type="text" class="pr-id" value="${pr.prId}">
					<label>权限id:</label><input type="text" class="permission-id" value="${permission.id}">
				</div>
			</c:forEach>
		</form>
	</div>
	
</body>
</html>