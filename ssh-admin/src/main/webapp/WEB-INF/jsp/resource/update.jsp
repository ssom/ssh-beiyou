<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h2>资源修改</h2>
	<div>
		<form action="<%=request.getContextPath()%>/resource/update" method="post">
			<input name="id" type="hidden" value="${resource.id}">
			<input name="name" type="text" value="${resource.name}"><br>
			<input name="url" type="text" value="${resource.url}"><br>
			<input type="submit" value="修改">
		</form>
	</div>
</body>
</html>